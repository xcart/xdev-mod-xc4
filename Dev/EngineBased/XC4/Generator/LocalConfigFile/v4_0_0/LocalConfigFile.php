<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */
namespace XDev\Module\XC4\Dev\EngineBased\XC4\Generator\LocalConfigFile\v4_0_0;

use XDev\Utils\Filesystem;

/**
 * Class Generator
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class LocalConfigFile extends \XDev\Module\XC4\Dev\EngineBased\XC4\Generator\LocalConfigFile\ALocalConfigFile
{
    public function getWebDir()
    {
        $web_dir = $this->getOption(self::PARAM_WEB_DIR);
        $web_dir = empty($web_dir) ? '' : '/' . $web_dir;

        return $web_dir;
    }

    public function generateFile()
    {

        $content = <<<END
<?php

//define('DEVELOPMENT_MODE', true);

\$xcart_http_host = '{$this->getOption(self::PARAM_HTTP_HOST)}';
\$xcart_https_host = '{$this->getOption(self::PARAM_HTTP_HOST)}';

\$sql_host = '{$this->getOption(self::PARAM_DB_HOST)}';
\$sql_user = '{$this->getOption(self::PARAM_DB_USER)}';
\$sql_db = '{$this->getOption(self::PARAM_DB_NAME)}';
\$sql_password = '{$this->getOption(self::PARAM_DB_PASSWORD)}';

\$xcart_web_dir = '{$this->getWebDir()}';

\$XCART_SESSION_NAME = substr(md5(\$storeid), 8, 5);

END;
        return Filesystem::dumpFile(\XDev\EM::get('Config')->getConfigLocalFilename(), $content);

    }
}
