<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\Processor\LocalDeploy\Step\CreateDemoUsers\v4_0_0;

use XDev\Base\Processor\AStep;
use Symfony\Component\Console\Input\ArrayInput;

/**
 * Class CreateDemoUsers
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class CreateDemoUsers extends AStep
{
    public function getTitle()
    {
        return 'Creating test users';
    }

    public function isOneTimeRun()
    {
        return false;
    }

    public function run()
    {
        $command = \XDev\Application::getInstance()->getSCApp()->find('users:create-demo');

        $this->getOutput()->writeln('');

        $command->run(new ArrayInput([]), $this->getOutput());

        \XDev\Core\Hook::invoke('deploy:after-create-users');
    }
}
