<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_4_0;

use XDev\Core\JsonAPI;

/**
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Create extends \XDev\Module\XC4\Dev\EngineBased\XC4\NativeAPI\API\Users\Create\v4_0_0\Create
{
    protected $isXCProEdition = null;

    public function process($params = [])
    {
        global $sql_tbl;

        parent::process($params);

        $adress_book = $this->getAddressBook();

        foreach ($adress_book as $data) {
            $data['userid'] = $this->getCreatedUserId();
            func_array2insert('address_book', $data);
        }

    }

    protected function getUserDataFields() {
        return [
            'login',
            'username',
            'usertype',
            'title',
            'firstname',
            'lastname',
            'company',
            'email',
            'status',
            'password',
        ];
    }

    protected function getAddressBookDataFields() {

        return [
            'title',
            'firstname',
            'lastname',
            'address',
            'city' ,
            'county',
            'state',
            'country',
            'zipcode',
            'zip4',
            'phone',
            'fax',
        ];

    }

    protected function getAddressBook()
    {
        $result = [];

        foreach (['b', 's'] as $type)
        {

            foreach ($this->getAddressBookDataFields() as $field) {
                $param_name = $type . '_' . $field;

                if (isset($this->params[$param_name])) {
                    $result[$type][$field] = $this->params[$param_name];
                }
            }

            if (!isset($result[$type])) {
                continue;
            }

            if ($type == 'b') {
                $result[$type]['default_b'] = 'Y';
                $result[$type]['default_s'] = 'N';
            } else {
                $result[$type]['default_b'] = 'N';
                $result[$type]['default_s'] = 'Y';
            }

            if (!$this->params['ship2diff']) {
                $result[$type]['default_b'] = 'Y';
                $result[$type]['default_s'] = 'Y';
            }

        }

        return $result;

    }

}
