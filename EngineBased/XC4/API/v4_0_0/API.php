<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\EngineBased\XC4\API\v4_0_0;

use XDev\EM;

/**
 * Class API
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class API extends \XDev\Base\AAPI
{
    protected $db;
    protected $isXCProEdition;

    public function getVersion()
    {
        return $this->getDbConfigParam('version');
    }

    public function getSoftwareName()
    {
        return 'X-Cart ' . $this->getVersion() . ' ' . ($this->isXCProEdition() ? 'PRO' : 'GOLD');
    }

    protected function isXCProEdition()
    {
        global $sql_tbl;

        if ($this->isXCProEdition === null) {
            $this->isXCProEdition = $this->getDb()->querySingleScalar("SELECT COUNT(*) FROM xcart_modules WHERE module_name = 'Simple_Mode'") ? true : false;
        }

        return $this->isXCProEdition;
    }

    public function getDbConfigParam($name)
    {
        $config = EM::get('Config');

        $method_name = 'getDbConfigParam' . ucfirst($name);

        $param_name = $config->$method_name();

        return $this->getDb()->querySingleScalar(
            'SELECT value FROM ' 
            . $config->getConfigTable()
            ." WHERE name='$param_name'"
        );
    }

    public function getSoftwareHttpHost()
    {
        return EM::get('Config')->getHttpHost();
    }

    public function getSoftwareHttpsHost()
    {
        return  EM::get('Config')->getHttpsHost();
    }
    public function getSoftwareStorefrontUrl()
    {
        return $this->getSoftwareWebBaseURL() . '/home.php';
    }

    public function getSoftwareAdminZoneUrl()
    {
        return $this->getSoftwareWebBaseURL() . '/admin';
    }

    public function getSoftwareWebDir()
    {
        return EM::get('Config')->getWebDir();
    }

    public function testHttps()
    {
        $result = file_get_contents($this->getSoftwareWebBaseURL(true) . '/home.php');

        return $result !== false;
    }

    public function getDb()
    {
        if (!$this->db) {

            if (\XDev::isInitialized()) {
                $this->db = \XDev::getDB();

            } else {
                $config = EM::get('Config');

                $host       = $config->getDbHost();
                $database   = $config->getDbName();
                $username   = $config->getDbUsername();
                $password   = $config->getDbPassword();
                $port       = $config->getDbPort() ? $config->getDbPort() : null;
                $socket     = $config->getDbSocket() ? $config->getDbSocket() : null;

                $this->db = new \XDev\Core\DbConnection($host, $username, $password, $database, $port, $socket);
            }

        }

        return $this->db;
    }
}
