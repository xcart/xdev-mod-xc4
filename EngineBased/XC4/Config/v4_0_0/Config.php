<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\EngineBased\XC4\Config\v4_0_0;

/**
 * Class Config
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class Config {
    static $FILENAME_CONFIG = 'config.php';
    static $FILENAME_CONFIG_LOCAL = 'config.local.php';

    static $VAR_HTTP_HOST       = 'xcart_http_host';
    static $VAR_HTTPS_HOST      = 'xcart_https_host';
    static $VAR_WEB_DIR         = 'xcart_web_dir';
    static $VAR_DB_HOST         = 'sql_host';
    static $VAR_DB_NAME         = 'sql_db';
    static $VAR_DB_USERNAME     = 'sql_user';
    static $VAR_DB_PASSWORD     = 'sql_password';

    static $TBL_CONFIG = 'config';

    static $DB_CONFIG_PARAM_VERSION = 'version';

    private $configLocalExists = false;

    private $configParser;
    private $configLocalParser;

    private $parsedVars = false;

    public function __construct() {
        $this->configLocalExists = file_exists($this->getConfigLocalFilename());
    }

    public function getDbConfigParamVersion() {
        return static::$DB_CONFIG_PARAM_VERSION;
    }

    public function getTablePrefix() {
        return 'xcart_';
    }

    public function getConfigTable() {
        return $this->getTablePrefix() . self::$TBL_CONFIG;
    }

    public function getConfigFilename() {
        return \XDev::getSoftwareDir() . \XDev::DS . self::$FILENAME_CONFIG;
    }

    public function getConfigLocalFilename() {
        return \XDev::getSoftwareDir() . \XDev::DS . self::$FILENAME_CONFIG_LOCAL;
    }

    public function getVar($name)
    {
        if (isset($this->parsedVars[$name])) {
            return $this->parsedVars[$name];
        }

        if ($this->configLocalExists){
            if (!$this->configLocalParser) {
                $this->configLocalParser = new \XDev\Utils\PhpParser();
                $this->configLocalParser->load($this->getConfigLocalFilename());
            }

            $result =  $this->configLocalParser->parseVarValue($name);

            if ($result !== false) {
                $this->parsedVars[$name] = $result;

                return $this->parsedVars[$name];
            }
        }

        if (!$this->configParser) {
            $this->configParser =  new \XDev\Utils\PhpParser();
            $this->configParser->load($this->getConfigFilename());
        }

        $result = $this->configParser->parseVarValue($name);

        if ($result !== false) {
            $this->parsedVars[$name] = $result;

            return $this->parsedVars[$name];

        } else {
            throw new \Exception("No variable named '$name' found in the configuration file");
        }

    }

    public function getHttpHost() {
        return $this->getVar(static::$VAR_HTTP_HOST);
    }

    public function getHttpsHost() {
        return $this->getVar(static::$VAR_HTTPS_HOST);
    }

    public function getWebDir() {
        return $this->getVar(static::$VAR_WEB_DIR);
    }

    public function getDbHost() {
        return $this->getVar(static::$VAR_DB_HOST);
    }

    public function getDbName() {
        return $this->getVar(static::$VAR_DB_NAME);
    }

    public function getDbUsername() {
        return $this->getVar(static::$VAR_DB_USERNAME);
    }

    public function getDbPassword() {
        return $this->getVar(static::$VAR_DB_PASSWORD);
    }

    public function getDbPort() {
        return '';
    }

    public function getDbSocket() {
        return '';
    }

}
