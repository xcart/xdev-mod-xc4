<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\EngineBased\XC4\ConfigPatcher\v4_0_0;

/**
 * Class ConfigPatcher
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ConfigPatcher extends \XDev\Utils\RegexPatcher
{
    const REGEXP_VAR_SQL_HOST           = '/\$sql_host\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_SQL_DB             = '/\$sql_db\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_SQL_PASS           = '/\$sql_password\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_XC_SESS_NAME       = '/\$XCART_SESSION_NAME\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_ADMIN_ALLOWED_IP   = '/\$admin_allowed_ip\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_INSTALL_AUTH_CODE  = '/\$installation_auth_code\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_XC_BLOWFIST_KEY    = '/\$blowfish_key\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';

    public function applyPatchClearSecureData()
    {
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_SQL_HOST, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_SQL_DB, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_SQL_PASS, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SESS_NAME, 'xid_');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_ADMIN_ALLOWED_IP, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_INSTALL_AUTH_CODE, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_BLOWFIST_KEY, 'dev');
    }

    public function applyPatchGenerateSecureData($blowfish)
    {
        // TODO: use XC-like algorithms for keys generation
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SESS_NAME, 'xid_' .  substr(md5($blowfish), 0, 5));
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_BLOWFIST_KEY, md5($blowfish));
    }

}
