<?php

/**
 * Copyright (c) 2011-present Qualiteam software Ltd. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 *
 */

namespace XDev\Module\XC4\EngineBased\XC4\ConfigPatcher\v4_5_5;

/**
 * Class ConfigPatcher
 *
 * @author Pavel Gavrilenko <barni@x-cart.com>
 */
class ConfigPatcher extends \XDev\Module\XC4\EngineBased\XC4\ConfigPatcher\v4_0_0\ConfigPatcher
{
    const REGEXP_VAR_XC_SECURITY_KEY_SESS       = '/\$xc_security_key_session\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_XC_SECURITY_KEY_CONFIG     = '/\$xc_security_key_config\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_VAR_XC_SECURITY_KEY_GENERAL    = '/\$xc_security_key_general\s*?\=\s*[\'\"]?([^\s\'\"]*)[\'\"]?\s*;/';
    const REGEXP_CONST_CHECK_CONFIG_INTEGRITY   = '/const\s*?CHECK_CONFIG_INTEGRITY\s*\=\s*([^\s\'\"]*)\s*;/';

    public function applyPatchClearSecureData()
    {
        parent::applyPatchClearSecureData();

        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SECURITY_KEY_SESS, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SECURITY_KEY_CONFIG, '');
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SECURITY_KEY_GENERAL, '');
    }

    public function applyPatchDisableCheckConfigIntegrity() {
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_CONST_CHECK_CONFIG_INTEGRITY, 'FALSE');
    }

    public function applyPatchGenerateSecureData($blowfish)
    {
        parent::applyPatchGenerateSecureData($blowfish);

        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SECURITY_KEY_SESS, md5($blowfish));
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SECURITY_KEY_CONFIG, md5($blowfish));
        $this->patchedContent = self::applyPatch($this->patchedContent, self::REGEXP_VAR_XC_SECURITY_KEY_GENERAL, md5($blowfish));

    }

}

