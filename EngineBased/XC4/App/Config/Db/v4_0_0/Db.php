<?php

namespace XDev\Module\XC4\EngineBased\XC4\App\Config\Db\v4_0_0;

use XDev\EM;

class Db extends \XDev\Config\Db
{

    protected function getDefaultConfigData() {
        $config = EM::get('Config');

        return [
            'host'      => $config->getDbHost(),
            'database'  => $config->getDbName(),
            'username'  => $config->getDbUsername(),
            'password'  => $config->getDbPassword(),
            'port'      => $config->getDbPort(),
            'socket'    => $config->getDbSocket(),
        ];
    }

}
