<?php

namespace XDev\Module\XC4\EngineBased\XC4\App\Config\DbDump\v4_4_0;

class DbDump extends \XDev\Module\XC4\EngineBased\XC4\App\Config\DbDump\v4_0_0\DbDump
{

    protected function getDefaultConfigData() {
        $data = parent::getDefaultConfigData();

        $data[self::PARAM_EXCLUDE_TABLES][] = 'xcart_address_book';

        return $data;
    }

}
